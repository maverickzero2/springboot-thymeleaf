package com.mrapry.springboot.controller.admin;

import com.mrapry.springboot.services.entity.User;
import com.mrapry.springboot.services.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mrapry on 2/7/17.
 */
@Controller
@RequestMapping(value = "/admin/user")
public class UserController {
    //  LOGGER
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index(ModelMap mm){
        mm.addAttribute("user",userService.findAll());
        logger.info("--=== {} ===--", "Find All User");
        System.out.println(userService.findAll());
        return "/user/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addUser(){
        return "user/add";
    }

    @RequestMapping(value="/{key}/{value}", method = RequestMethod.GET)
    public String findByUserUser (@PathVariable String key, @PathVariable String value){
        Map mm = new HashMap();
        if(key.equalsIgnoreCase("id")){
            logger.info("--=== {} [{}]===--", "Find User byId", value);
            mm.put("user", userService.findOne(value));
            return "/user/list";
        } else if(key.equalsIgnoreCase("user")){
            logger.info("--=== {} [{}]===--", "Find User byUsername", value);
            mm.put("user", userService.findByUser(value));
            return "/user/list";
        } else if(key.equalsIgnoreCase("email")){
            logger.info("--=== {} [{}]===--", "Find User byUsername", value);
            mm.put("user", userService.findByEmail(value));
            return "/user/list";
        } else {
            logger.error("--=== {} ===--", "Invalid Search Key Parameter. allowed key is [id, username]");
            mm.put("user", null);
            return "/user/list";
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public void save(@RequestBody User user){
        userService.save(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable String id, @RequestBody User user){
        User u = userService.findOne(id);
        if(u!=null){
            user.setId(u.getId());
            logger.info("--=== {} [{}]===--", "Update User", user.toString());
            userService.save(user);
        }
        logger.info("--=== {} [{}]===--", "User not found ! byId ", id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id){
        User u = userService.findOne(id);
        if(u!=null){
            logger.info("--=== {} [{}]===--", "Delete User", id);
            userService.delete(u);
        }
        logger.info("--=== {} [{}]===--", "For Delete, User not found ! byId ", id);
    }

}
