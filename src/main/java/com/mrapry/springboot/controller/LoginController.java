package com.mrapry.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by mrapry on 2/5/17.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(){

    }

    @RequestMapping(value = "/login_error", method = RequestMethod.GET)
    public void error(){

    }

}
